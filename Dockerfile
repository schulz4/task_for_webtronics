FROM python:3.10

RUN mkdir /task

WORKDIR /task

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

RUN chmod a+x /task/docker/*.sh

#CMD ["gunicorn", "app.main:app", "--workers", "2", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind=0.0.0.0:8000"]
