from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.database import async_session_maker


async def get_session() -> AsyncSession:
    async with async_session_maker() as session:
        yield session
        await session.commit()


def get_repository(repository):
    def _get_repository(session: AsyncSession = Depends(get_session)):
        return repository(session)

    return _get_repository
