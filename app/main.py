from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.auth.router import auth_router, user_router
from app.posts.router import post_router

app = FastAPI()

app.include_router(auth_router)
app.include_router(user_router)
app.include_router(post_router)

# CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:9000",
        "http://127.0.0.1:9000",
                   ],
    allow_credentials=True,
    allow_methods=["GET", "POST", "DELETE", "PATCH", "PUT"],
    allow_headers=[
        "Content-Type", "Set-Cookie", "Access-Control-Allow-Headers",
        "Access-Control-Allow-Origin", "Authorization"
    ],
    max_age=1
)
