from sqlalchemy import select, delete, update
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from app.logger import logger


class BaseRepo:
    def __init__(self, session: AsyncSession):
        self.session = session
        self.model = None

    async def get_all(self, *clauses):
        query = select(self.model).where(*clauses)
        result = await self.session.execute(query)
        return result.scalars().all()

    async def find_one_or_none(self, **filter_by):
        query = select(self.model).filter_by(**filter_by)
        result = await self.session.execute(query)
        return result.scalar_one_or_none()

    async def add(self, **values):
        query = insert(self.model).values(**values)
        await self.session.execute(query)
        await self.session.commit()

    async def delete(self, **filter_by):
        query = delete(self.model).filter_by(**filter_by).returning(self.model)
        result = await self.session.execute(query)
        await self.session.commit()
        return result.scalars().first()

    async def update(self, *clauses, **values):
        query = update(self.model).where(*clauses).values(**values).returning(self.model)
        await self.session.execute(query)
        await self.session.commit()
