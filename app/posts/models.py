from datetime import datetime
from typing import List, Literal
from uuid import uuid4

from sqlalchemy import UUID, TEXT, TIMESTAMP, ARRAY, VARCHAR, INTEGER, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base import Base


class Post(Base):
    __tablename__ = "posts"

    id: Mapped[UUID] = mapped_column(UUID, primary_key=True, default=uuid4)
    user_id: Mapped[UUID] = mapped_column(UUID, ForeignKey("users.id", ondelete="CASCADE"))
    text: Mapped[str] = mapped_column(TEXT, nullable=False)
    created_at: Mapped[datetime] = mapped_column(TIMESTAMP, default=datetime.utcnow)
    updated_at: Mapped[datetime] = mapped_column(TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow)
    tags: Mapped[List[str]] = mapped_column(ARRAY(VARCHAR), nullable=True)

    user: Mapped["User"] = relationship(back_populates="posts")


class LikeDislike(Base):
    __tablename__ = "likes_dislikes"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True)
    user_id: Mapped[UUID] = mapped_column(UUID, ForeignKey("users.id", ondelete="SET NULL"))
    post_id: Mapped[UUID] = mapped_column(UUID, ForeignKey("posts.id", ondelete="CASCADE"))
    status: Mapped[Literal["like", "dislike"]] = mapped_column(VARCHAR(10))

    user: Mapped["User"] = relationship()
    post: Mapped["Post"] = relationship()
