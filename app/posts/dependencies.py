from fastapi.params import Depends

from app.dependencies import get_repository
from app.posts.repository import PostRepo, LikeDislikeRepo
from app.posts.services import PostService


def get_post_service(
        post_repo: PostRepo = Depends(get_repository(PostRepo)),
        like_dislike_repo: LikeDislikeRepo = Depends(get_repository(LikeDislikeRepo))
):
    return PostService(post_repo, like_dislike_repo)
