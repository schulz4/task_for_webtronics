from datetime import datetime
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, Field, validator


class SPostCreate(BaseModel):
    text: str
    tags: Optional[List[str]] = None


class SPostUpdate(BaseModel):
    text: Optional[str]
    tags: Optional[List[str]]
    

class SPostResponse(BaseModel):
    id: UUID
    message: str = Field(default="successful")
    

class SPost(SPostCreate):
    id: UUID
    user_id: UUID
    created_at: datetime
    updated_at: datetime
    likes: int = Field(default=0, ge=0)
    dislikes: int = Field(default=0, ge=0)

    class Config:
        orm_mode = True

    @validator("created_at", "updated_at")
    def remove_seconds_and_milliseconds(cls, value: datetime):
        if isinstance(value, datetime):
            formatted_date = value.strftime("%d:%m:%Y %H:%M")
            return datetime.strptime(formatted_date, "%d:%m:%Y %H:%M")
        else:
            return value
