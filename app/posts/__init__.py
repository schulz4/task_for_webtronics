from app.posts.models import Post, LikeDislike

__all__ = [
    "Post",
    "LikeDislike"
]
