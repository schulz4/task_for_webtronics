from typing import List
from uuid import UUID

from fastapi import APIRouter, status
from fastapi.params import Depends

from app.auth.dependencies import get_current_user
from app.auth.models import User
from app.posts.dependencies import get_post_service
from app.posts.exceptions import NotUsersPostException
from app.posts.schemas import SPostCreate, SPost, SPostResponse, SPostUpdate
from app.posts.services import PostService

post_router = APIRouter(
    prefix="/posts",
    tags=["Post"]
)


@post_router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    description="Creation new post"
)
async def create_post(
        post: SPostCreate,
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> SPostResponse:
    post = await post_service.add_new_post(
        user_id=current_user.id,
        text=post.text,
        tags=post.tags
    )
    return SPostResponse(id=post.id, message="Post was successfully created")


@post_router.get(
    "/{post_id}",
    description="Get post by id"
)
async def get_post(post_id: UUID, post_service: PostService = Depends(get_post_service)) -> SPost:
    post = await post_service.get_posts_by_id(post_id)
    return post


@post_router.delete(
    "/{post_id}",
    description="Deleting user's post by id"
)
async def delete_post(
        post_id: UUID,
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> SPostResponse:
    check = await post_service.check_post_belong_to_user(current_user, post_id)
    if not check:
        raise NotUsersPostException

    await post_service.delete_post(post_id, current_user.id)
    return SPostResponse(id=post_id, message="Post was successfully deleted")


@post_router.put(
    "/{post_id}",
    description="Editing user's post by id"
)
async def update_post(
        post_id: UUID,
        post: SPostUpdate,
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> SPostResponse:
    data = post.dict(exclude_none=True)
    check = await post_service.check_post_belong_to_user(current_user, post_id)
    if not check:
        raise NotUsersPostException
    
    await post_service.edit_post(
        post_id=post_id,
        user_id=current_user.id,
        **data
    )

    return SPostResponse(id=post_id, message="Post was successfully updated")


@post_router.post(
    "/{post_id}/like",
    description="Like post by id if it not belongs to current user"
)
async def like_post(
        post_id: UUID,
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> SPostResponse:
    await post_service.like_or_dislike_post(post_id, current_user, "likes")
    return SPostResponse(id=post_id)


@post_router.post(
    "/{post_id}/dislike",
    description="Dislike post by id if it not belongs to current user"
)
async def dislike_post(
        post_id: UUID,
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> SPostResponse:
    await post_service.like_or_dislike_post(post_id, current_user, "dislikes")
    return SPostResponse(id=post_id)


@post_router.get(
    "",
    description="Get all current user's posts"
)
async def get_current_user_posts(
        post_service: PostService = Depends(get_post_service),
        current_user: User = Depends(get_current_user)
) -> List[SPost]:
    posts = await post_service.get_posts_by_user(current_user.id)
    return posts
