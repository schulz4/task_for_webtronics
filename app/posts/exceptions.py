from fastapi import status

from app.exceptions import AppException


class CantAddNewPostException(AppException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Can't add new post"


class PostNotExistException(AppException):
    status_code = status.HTTP_404_NOT_FOUND
    detail = "Post not exist"


class CantUpdatePostException(AppException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Can't update post"


class CantDeletePostException(AppException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Can't delete post"


class NotUsersPostException(AppException):
    status_code = status.HTTP_409_CONFLICT
    detail = "User can't delete/update this post. It doesn't exist or doesn't belong to user"


class UserCantLikeOwnPostException(AppException):
    status_code = status.HTTP_409_CONFLICT
    detail = "User can't like or dislike own post"
