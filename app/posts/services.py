from typing import Literal, List

from sqlalchemy.exc import SQLAlchemyError

from app.auth import User
from app.posts.exceptions import CantUpdatePostException, CantDeletePostException, CantAddNewPostException, \
    UserCantLikeOwnPostException, PostNotExistException
from app.posts.repository import PostRepo, LikeDislikeRepo
from app.posts.schemas import SPost
from app.redis.rediscli import RedisCli


class PostService:
    def __init__(self, post_repo: PostRepo, like_dislike_repo: LikeDislikeRepo) -> None:
        self._post_repo = post_repo
        self._like_dislike_repo = like_dislike_repo
        self._redis_cli = RedisCli()

    async def add_new_post(self, **values) -> SPost:
        try:
            post = await self._post_repo.add(**values)
        except SQLAlchemyError:
            raise CantAddNewPostException

        serialized_post = SPost(**post)
        await self._redis_cli.add(f"Post:{serialized_post.id}:{serialized_post.user_id}", serialized_post.json())

        return serialized_post

    async def edit_post(self, post_id, user_id, **values) -> None:
        try:
            updated_post = await self._post_repo.update(self._post_repo.model.id == post_id, **values)
            serialized_post = SPost(**updated_post)
            await self._redis_cli.update(f"Post:{post_id}:{user_id}", serialized_post.json())
        except SQLAlchemyError:
            raise CantUpdatePostException

    async def delete_post(self, post_id, user_id) -> None:
        try:
            await self._post_repo.delete(id=post_id)
            await self._redis_cli.delete(f"Post:{post_id}:{user_id}")
        except SQLAlchemyError:
            raise CantDeletePostException

    async def get_posts_by_id(self, post_id) -> SPost:
        post = await self._redis_cli.get(f"Post:{post_id}:*")
        if not post:
            raise PostNotExistException
        serializer_post = SPost.parse_raw(post)
        return serializer_post

    async def get_posts_by_user(self, user_id) -> List[SPost]:
        posts = await self._redis_cli.mget(f"*{user_id}")
        print(posts)
        serialized_posts = [SPost.parse_raw(i) for i in posts]
        return serialized_posts

    @staticmethod
    async def check_post_belong_to_user(user: User, post_id) -> bool:
        posts = await user.awaitable_attrs.posts
        if post_id not in [post.id for post in posts]:
            return False
        else:
            return True

    async def like_or_dislike_post(self, post_id, user: User, status: Literal["likes", "dislikes"]) -> None:
        check = await self.check_post_belong_to_user(user=user, post_id=post_id)
        if check:
            raise UserCantLikeOwnPostException

        db_post = await self._post_repo.find_one_or_none(id=post_id)
        redis_post = await self._redis_cli.get(f"Post:{post_id}:*")
        if not redis_post:
            raise PostNotExistException

        serialized_post: dict = SPost.parse_raw(redis_post).dict()
        like_or_dislike = await self._like_dislike_repo.find_one_or_none(post_id=post_id, user_id=user.id)

        if not like_or_dislike:
            serialized_post[status] += 1
            await self._like_dislike_repo.add(
                post_id=post_id,
                user_id=user.id,
                status=status
            )
            print(serialized_post)
        elif status == like_or_dislike.status:
            serialized_post[status] -= 1
            print(serialized_post)
            await self._like_dislike_repo.delete(post_id=post_id, user_id=user.id)
            print(serialized_post)
        else:
            print("HI")
            if status == "likes":
                serialized_post["likes"] += 1
                serialized_post["dislikes"] -= 1
            else:
                serialized_post["likes"] -= 1
                serialized_post["dislikes"] += 1
            print(serialized_post)

            await self._like_dislike_repo.update(
                self._like_dislike_repo.model.post_id == post_id,
                self._like_dislike_repo.model.user_id == user.id,
                status=status
            )

        to_json = SPost(**serialized_post).json()
        await self._redis_cli.update(f"Post:{post_id}:{db_post.user_id}", to_json)
