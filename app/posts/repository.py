from sqlalchemy import insert, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.base_repository import BaseRepo
from app.posts.models import Post, LikeDislike


class PostRepo(BaseRepo):
    def __init__(self, session: AsyncSession):
        super().__init__(session=session)
        self.model = Post

    async def add(self, **values):
        query = insert(self.model).values(**values).returning(
            self.model.id,
            self.model.user_id,
            self.model.text,
            self.model.tags,
            self.model.created_at,
            self.model.updated_at
        )
        result = await self.session.execute(query)
        await self.session.commit()
        return result.mappings().first()

    async def update(self, *clauses, **values):
        query = update(self.model).where(*clauses).values(**values).returning(
            self.model.id,
            self.model.user_id,
            self.model.text,
            self.model.tags,
            self.model.created_at,
            self.model.updated_at
        )
        result = await self.session.execute(query)
        await self.session.commit()
        return result.mappings().first()


class LikeDislikeRepo(BaseRepo):
    def __init__(self, session: AsyncSession):
        super().__init__(session=session)
        self.model = LikeDislike
