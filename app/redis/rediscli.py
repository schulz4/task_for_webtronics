from redis import asyncio as aioredis

from app.config import get_settings


class RedisCli:
    def __init__(self):
        self._redis = aioredis.from_url(get_settings().redis.redis_url)

    async def add(self, key: str, value) -> None:
        await self._redis.append(key, value)

    async def get(self, pattern: str):
        keys = await self._redis.keys(pattern)
        return await self._redis.get(keys[0])

    async def delete(self, key):
        await self._redis.delete(key)

    async def update(self, key, value):
        await self._redis.set(key, value)

    async def mget(self, pattern):
        keys = await self._redis.keys(pattern)
        if not keys:
            return []
        return await self._redis.mget(*keys)
