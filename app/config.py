from dataclasses import dataclass
from typing import Literal

from pydantic import BaseSettings


class BaseConfig(BaseSettings):
    MODE: Literal["DEV", "TEST", "PROD"]
    LOG_LEVEL: Literal["DEBUG", "INFO", "WARN", "ERROR", "FATAL"]

    class Config:
        env_file = ".env"


class DBSetting(BaseConfig):
    DB_HOST: str
    DB_NAME: str
    DB_PORT: int
    DB_PASS: str
    DB_USER: str

    @property
    def _prefix(self):
        return "test_" if self.MODE == "TEST" else ""

    @property
    def database_url(self):

        user = f"{self.DB_USER}:{self.DB_PASS}"
        database = f"{self.DB_HOST}:{self.DB_PORT}/{self._prefix}{self.DB_NAME}"
        return f"postgresql+asyncpg://{user}@{database}"


class AuthSetting(BaseConfig):
    ALGORITHM: str
    KEY: str


class RedisSetting(BaseConfig):
    REDIS_HOST: str
    REDIS_PORT: int

    @property
    def redis_url(self):
        return f"redis://{self.REDIS_HOST}:{self.REDIS_PORT}"


class TokenSettings(BaseConfig):
    ACCESS_TOKEN_EXPIRES: int
    REFRESH_TOKEN_EXPIRES: int


class EmailHunterSettings(BaseConfig):
    API_KEY: str


@dataclass
class Settings:
    base: BaseConfig
    db_setting: DBSetting
    auth: AuthSetting
    redis: RedisSetting
    tokens: TokenSettings
    email_hunter: EmailHunterSettings


def get_settings():
    return Settings(
        base=BaseConfig(),
        db_setting=DBSetting(),
        auth=AuthSetting(),
        redis=RedisSetting(),
        tokens=TokenSettings(),
        email_hunter=EmailHunterSettings()
    )
