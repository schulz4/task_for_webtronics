from app.apis.base import BaseClient


class EmailHunterCli(BaseClient):
    def __init__(self, api_key: str = None):
        self.api_key = api_key
        self.base_url = "https://api.hunter.io"
        super().__init__(base_url=self.base_url)

    async def verify_email(self, email: str) -> dict:

        response = await self._make_request(
            method="get",
            url="/v2/email-verifier",
            params={"email": email, "api_key": self.api_key},
        )
        return response[1].get("data")
