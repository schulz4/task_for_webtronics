from __future__ import annotations

import asyncio
import ssl
from typing import TYPE_CHECKING, Any

from aiohttp import ClientError, ClientSession, FormData, TCPConnector
from ujson import dumps, loads

if TYPE_CHECKING:
    from collections.abc import Mapping

    from yarl import URL


class BaseClient:

    def __init__(self, base_url: str | URL) -> None:
        self._base_url = base_url
        self._session: ClientSession | None = None

    async def _get_session(self) -> ClientSession:
        """Get aiohttp session with cache."""
        if self._session is None:
            ssl_context = ssl.SSLContext()
            connector = TCPConnector(ssl_context=ssl_context)
            self._session = ClientSession(
                base_url=self._base_url,
                connector=connector,
                json_serialize=dumps,
            )

        return self._session

    async def _make_request(
            self,
            method: str,
            url: str | URL,
            params: Mapping[str, str] | None = None,
            json: Mapping[str, str] | None = None,
            headers: Mapping[str, str] | None = None,
            data: FormData | None = None,
    ) -> tuple[int, dict[str, Any]]:
        """Make request and return decoded json response."""
        session = await self._get_session()

        async with session.request(
                method, url, params=params, json=json, headers=headers, data=data
        ) as response:
            status = response.status
            if status != 200:
                s = await response.text()
                raise ClientError(f"Got status {status} for {method} {url}: {s}")
            try:
                result = await response.json(loads=loads)
            except Exception as e:
                raise e

        return status, result

    async def close(self) -> None:
        """Graceful session close."""
        if not self._session:
            return

        if self._session.closed:
            return

        await self._session.close()

        # Wait 250 ms for the underlying SSL connections to close
        await asyncio.sleep(0.25)
