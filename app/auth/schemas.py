from datetime import date
from typing import Literal
from uuid import UUID

from pydantic import BaseModel, EmailStr


class SUserAuth(BaseModel):
    email: EmailStr
    password: str


class SUser(BaseModel):
    id: UUID
    email: EmailStr
    hashed_password: str

    class Config:
        orm_mode = True


class TokenInfo(BaseModel):
    token_name: Literal["refresh", "access"]
    token: str
    expires_in: date


class SAccessToken(BaseModel):
    access_token: str
    token_type: str


class STokens(BaseModel):
    refresh_token: TokenInfo
    access_token: TokenInfo
