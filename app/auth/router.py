from typing import Annotated

from fastapi import APIRouter, Depends, Response, Request, status
from fastapi.security import OAuth2PasswordRequestForm

from app.auth.dependencies import get_auth_service, get_current_user
from app.auth.exceptions import TokenAbsentException, UserAlreadyExistsException, TokenExpiredException
from app.auth.models import User
from app.auth.repository import SessionRepo
from app.auth.schemas import STokens, SAccessToken, SUser, SUserAuth
from app.auth.services import AuthorizationService
from app.auth.utils import check_jwt
from app.dependencies import get_repository

auth_router = APIRouter(
    prefix="/auth",
    tags=["Auth"]
)
user_router = APIRouter(
    prefix="/users",
    tags=["Users"]
)


@auth_router.post(
    "/register",
    status_code=status.HTTP_201_CREATED,
    description="User registration"
)
async def register_user(
        user_data: SUserAuth,
        auth_service: AuthorizationService = Depends(get_auth_service)
):
    await auth_service.verify_email(email=user_data.email)
    existing_user = await auth_service.check_user(email=user_data.email)
    if existing_user:
        raise UserAlreadyExistsException

    await auth_service.add_user(
        email=user_data.email,
        password=user_data.password
    )


@auth_router.post(
    "/login",
    description="User Login"
)
async def login(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
        response: Response,
        auth_service: AuthorizationService = Depends(get_auth_service)
) -> SAccessToken:
    user = await auth_service.authenticate_user(form_data.username, form_data.password)

    tokens: STokens = auth_service.create_tokens({"sub": str(user.id)})

    response.set_cookie(
        key=tokens.refresh_token.token_name,
        value=tokens.refresh_token.token,
        max_age=3600,
        path="/auth",
        httponly=True
    )

    await auth_service.create_refresh_session(
        user_id=user.id,
        refresh_token=tokens.refresh_token.token,
        expires_in=tokens.refresh_token.expires_in
    )

    return {
        "access_token": tokens.access_token.token,
        "token_type": "Bearer"
    }


@auth_router.post(
    "/refresh",
    description="Refresh tokens after access token have been expired"
)
async def get_refresh_token(
        request: Request,
        response: Response,
        auth_service: AuthorizationService = Depends(get_auth_service)
) -> SAccessToken:
    refresh_token = request.cookies.get("refresh")

    if not refresh_token:
        raise TokenAbsentException

    user_id = await auth_service.check_refresh_token(refresh_token)

    tokens: STokens = auth_service.create_tokens({"sub": str(user_id)})

    response.set_cookie(
        key=tokens.refresh_token.token_name,
        value=tokens.refresh_token.token,
        max_age=3600,
        path="/auth",
        httponly=True
    )

    await auth_service.create_refresh_session(
        user_id=user_id,
        refresh_token=tokens.refresh_token.token,
        expires_in=tokens.refresh_token.expires_in
    )

    return {
        "access_token": tokens.access_token.token,
        "token_type": "Bearer"
    }


@auth_router.post(
    "/logout",
    description="User logout"
)
async def logout(
        response: Response,
        user: User = Depends(get_current_user),
        session_repo: SessionRepo = Depends(get_repository(SessionRepo))
) -> dict:
    response.delete_cookie("refresh", path="auth/", httponly=True)
    await session_repo.delete(user_id=user.id)
    return {
        "logout": "success"
    }


@user_router.get(
    "/me",
    description="Information about current authorized user"
)
async def get_me(
        user: User = Depends(get_current_user)
) -> SUser:
    return user
