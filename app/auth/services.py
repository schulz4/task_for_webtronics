from datetime import datetime, timedelta
from typing import Optional, Literal

from jose import jwt
from passlib.context import CryptContext
from pydantic import EmailStr
from sqlalchemy.exc import SQLAlchemyError

from app.apis.emailhunter_cli import EmailHunterCli
from app.auth.exceptions import TokenNotAddedException, UserHasNotSession, IncorrectEmailOrPasswordException, \
    EmailInvalidException, TokenExpiredException
from app.auth.models import User
from app.auth.repository import UserRepo, SessionRepo
from app.auth.schemas import TokenInfo, STokens
from app.auth.utils import check_jwt
from app.config import get_settings


class AuthorizationService:
    def __init__(self, user_repo: UserRepo, session_repo: SessionRepo) -> None:
        self._user_repo = user_repo
        self._session_repo = session_repo
        self.settings = get_settings()
        self._email_hunter = EmailHunterCli(self.settings.email_hunter.API_KEY)
        self._pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def _create_token(self, token_name: Literal["refresh", "access"], exp_min: int, data: Optional[dict]) -> TokenInfo:
        to_encode = data.copy() if data else {}
        expire = datetime.utcnow() + timedelta(minutes=exp_min)
        to_encode.update(exp=expire)
        encode_jwt = jwt.encode(
            to_encode, self.settings.auth.KEY, self.settings.auth.ALGORITHM
        )
        return TokenInfo(token_name=token_name, token=encode_jwt, expires_in=expire)

    def create_tokens(self, data: dict) -> STokens:
        access_token = self._create_token(
            token_name="access",
            data=data,
            exp_min=self.settings.tokens.ACCESS_TOKEN_EXPIRES
        )
        refresh_token = self._create_token(
            token_name="refresh",
            data=data,
            exp_min=self.settings.tokens.REFRESH_TOKEN_EXPIRES
        )
        return STokens(access_token=access_token, refresh_token=refresh_token)

    async def verify_email(self, email: str) -> None:
        result = await self._email_hunter.verify_email(email)
        if result.get("status") == "invalid":
            raise EmailInvalidException

    async def authenticate_user(self, email: EmailStr, password: str) -> User:
        user: User = await self._user_repo.find_one_or_none(email=email)
        if not user or not self._verify_password(password, user.hashed_password):
            raise IncorrectEmailOrPasswordException
        return user

    async def check_user(self, email: EmailStr) -> User:
        user = await self._user_repo.find_one_or_none(email=email)
        return user

    async def add_user(self, email: EmailStr, password: str) -> None:
        hashed_password = self._get_password_hash(password)

        await self._user_repo.add(
            email=email,
            hashed_password=hashed_password
        )

    async def create_refresh_session(self, **values) -> None:
        try:
            await self._session_repo.add(**values)
        except SQLAlchemyError:
            raise TokenNotAddedException

    async def check_refresh_token(self, token: str) -> str:
        try:
            user_id = check_jwt(token=token)
            session = await self._session_repo.find_one_or_none(refresh_token=token, user_id=user_id)
        except TokenExpiredException:
            await self._session_repo.delete(id=session.id)
            raise TokenExpiredException

        if not session:
            raise UserHasNotSession

        await self._session_repo.delete(id=session.id)

        return user_id

    def _get_password_hash(self, password: str) -> str:
        return self._pwd_context.hash(password)

    def _verify_password(self, plain_password: str, hashed_password: str) -> bool:
        return self._pwd_context.verify(plain_password, hashed_password)
