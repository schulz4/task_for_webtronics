from typing import Annotated

from fastapi.params import Depends
from fastapi.security import OAuth2PasswordBearer

from app.auth.exceptions import NotUserID
from app.auth.repository import UserRepo, SessionRepo
from app.auth.services import AuthorizationService
from app.auth.utils import check_jwt
from app.dependencies import get_repository


def get_auth_service(
        user_repo: UserRepo = Depends(get_repository(UserRepo)),
        session_repo: SessionRepo = Depends(get_repository(SessionRepo))
):
    return AuthorizationService(user_repo, session_repo)


def get_oauth2_scheme():
    return OAuth2PasswordBearer(tokenUrl="auth/login")


async def get_current_user(
        token: Annotated[str, Depends(get_oauth2_scheme())],
        user_repo: UserRepo = Depends(get_repository(UserRepo))
):

    user_id = check_jwt(token)

    user = await user_repo.find_one_or_none(id=user_id)
    if not user:
        raise NotUserID

    return user
