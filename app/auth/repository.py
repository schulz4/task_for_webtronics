from app.auth.models import User, RefreshSession
from app.db.base_repository import BaseRepo


class UserRepo(BaseRepo):
    def __init__(self, session):
        super().__init__(session=session)
        self.model = User


class SessionRepo(BaseRepo):
    def __init__(self, session):
        super().__init__(session=session)
        self.model = RefreshSession
